<?php

namespace mohagames\guns;

use pocketmine\event\entity\ProjectileHitEntityEvent;
use pocketmine\event\entity\ProjectileHitEvent;
use pocketmine\utils\TextFormat;
use pocketmine\Server;
use pocketmine\Player;
use pocketmine\plugin\PluginBase;
use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\event\Listener;
use pocketmine\event\player\PlayerInteractEvent;
use pocketmine\event\player\PlayerItemHeldEvent;
use pocketmine\item\ItemIds;
use pocketmine\item\Bow;
use pocketmine\block\Block;
use pocketmine\level\Level;
use pocketmine\entity\Entity;
use pocketmine\nbt\tag\CompoundTag;
use pocketmine\entity\projectile\Arrow as ArrowEntity;
use pocketmine\entity\projectile\Projectile;
use pocketmine\event\entity\EntityShootBowEvent;
use pocketmine\nbt\tag\ListTag;
use pocketmine\nbt\tag\ByteTag;
use pocketmine\nbt\tag\DoubleTag;
use pocketmine\nbt\tag\FloatTag;
use pocketmine\item\Item;
use pocketmine\item\ItemFactory;
use pocketmine\math\Vector3;
use pocketmine\level\sound\GhastShootSound;
use pocketmine\level\sound\ClickSound;
use pocketmine\level\sound\PopSound;
use pocketmine\entity\Effect;
use pocketmine\entity\EffectInstance;
use pocketmine\level\particle\RedstoneParticle;

use function min;

class Main extends PluginBase implements Listener{
    public function onLoad() : void{
        $this->getLogger()->info(TextFormat::WHITE . "I've been loaded!");
    }
    public function onEnable() : void{
        $this->getServer()->getPluginManager()->registerEvents($this, $this);
        $this->getLogger()->info(TextFormat::DARK_GREEN . "I've been enabled!");
    }

    public function onCommand(CommandSender $sender, Command $command, string $label, array $args) : bool{
        switch($command->getName()){
            case "maakwapen":
                if(isset($args[0])) {
                    $guns = array("AK47", "AWP", "FP9");
                    if(in_array($args[0], $guns)){
                        $item = ItemFactory::get(Item::RECORD_13);
                        $item->clearCustomName();
                        $item->setCustomName("§4".$args[0]);
                        $sender->getInventory()->setItemInHand($item);
                    }else{
                        $sender->sendMessage("§4Gelieve de naam van het wapen op te geven.");
                        $sender->sendMessage("§f-§4AK47 §7(LMG)");
                        $sender->sendMessage("§f-§4AWP §7(Sniper)");
                        $sender->sendMessage("§f-§4FP9 §7(Shotgun)");
                    }
                }
                else{
                    $sender->sendMessage("§4Gelieve de naam van het wapen op te geven.");
                    $sender->sendMessage("§f-§4AK47 §7(LMG)");
                    $sender->sendMessage("§f-§4AWP §7(Sniper)");
                    $sender->sendMessage("§f-§4FP9 §7(Shotgun)");
                }
                return true;
            default:
                return false;
        }
    }
    public function onInteract(PlayerInteractEvent $e){
        $player = $e->getPlayer();
        if($e->getItem()->getId() == ItemIds::RECORD_13 AND $e->getItem()->getCustomName() == "§4AK47"){
            if($player->getInventory()->contains(ItemFactory::get(Item::ARROW, 0, 1)) || $player->getGamemode() == 1){
                $nbt = Entity::CreateBaseNBT($player->add(rand(-0.5, 0.5), 2, rand(-0.5, 0.5)), $player->getDirectionVector(), $player->yaw, $player->pitch );
                $diff = $player->getItemUseDuration();

                $p = $diff / 20;
                $baseForce = min((($p ** 2) + $p * 2) / 3, 1);
                $entity = new ArrowEntity($player->getLevel(), $nbt, $player, true);
                $entity->setPickupMode(0);
                $entity->setInvisible(true);
                $entity->setMotion($entity->getMotion()->multiply(2));
                $ev = new EntityShootBowEvent($player, new Bow, $entity, $baseForce * 110);
                $ev->call();
                $level = $e->getPlayer()->getLevel();
                $level->addSound(new GhastShootSound($e->getPlayer()->asVector3()));
                if($player->getGamemode() != 1) {
                    $player->getInventory()->removeItem(ItemFactory::get(Item::ARROW, 0, 1));
                }

            }

            else{
                $level = $e->getPlayer()->getLevel();
                $level->addSound(new ClickSound($e->getPlayer()->asVector3(), 3));
                $player->sendPopup("§4Geen munitie! Je kan munitie kopen bij een gunshop");
            }
        }
        if($e->getItem()->getId() == ItemIds::RECORD_13 AND $e->getItem()->getCustomName() == "§4AWP"){
            if($player->getInventory()->contains(ItemFactory::get(Item::ARROW, 0, 1)) || $player->getGamemode() == 1){
                $nbt = Entity::CreateBaseNBT(
                    $player->add(0, 2, 0),
                    $player->getDirectionVector(),
                    ($player->yaw > 180 ? 360 : 0) - $player->yaw,
                    -$player->pitch
                );
                $diff = $player->getItemUseDuration();
                $p = $diff / 20;

                $baseForce = min((($p ** 2) + $p * 2) / 3, 1);

                $entity = new ArrowEntity($player->getLevel(), $nbt, $player, true);
                $entity->setPickupMode(0);
                $entity->setInvisible(true);
                $entity->setMotion($entity->getMotion()->multiply(4));
                $ev = new EntityShootBowEvent($player, new Bow, $entity, $baseForce * 110);
                $ev->call();
                $level = $e->getPlayer()->getLevel();
                $level->addSound(new GhastShootSound($e->getPlayer()->asVector3()));
                if($player->getGamemode() != 1) {
                    $player->getInventory()->removeItem(ItemFactory::get(Item::ARROW, 0, 1));
                }
            }


            else{
                $level = $e->getPlayer()->getLevel();
                $level->addSound(new ClickSound($e->getPlayer()->asVector3(), 3));
                $player->sendPopup("§4Geen munitie! Je kan munitie kopen bij een gunshop");
            }
        }
        if($e->getItem()->getId() == ItemIds::RECORD_13 AND $e->getItem()->getCustomName() == "§4FP9"){
            if($player->getInventory()->contains(ItemFactory::get(Item::ARROW, 0, 3)) || $player->getGamemode() == 1){
                $xz = -0.5;
                for($i = 0; $i <= 2; $i++) {
                    $nbt = Entity::CreateBaseNBT(
                        $player->add($xz, 2, $xz),
                        $player->getDirectionVector(),
                        ($player->yaw > 180 ? 360 : 0) - $player->yaw,
                        -$player->pitch
                    );
                    $diff = $player->getItemUseDuration();
                    $p = $diff / 20;
                    $xz = $xz+0.5;
                    $baseForce = min((($p ** 2) + $p * 2) / 3, 1);

                    $entity = new ArrowEntity($player->getLevel(), $nbt, $player, true);
                    $entity->setPickupMode(0);
                    $entity->setInvisible(true);
                    $entity->setMotion($entity->getMotion()->multiply(1.1));
                    $ev = new EntityShootBowEvent($player, new Bow, $entity, $baseForce * 110);
                    $ev->call();
                }

                $level = $e->getPlayer()->getLevel();
                $level->addSound(new GhastShootSound($e->getPlayer()->asVector3()));
                if($player->getGamemode() != 1) {
                    $player->getInventory()->removeItem(ItemFactory::get(Item::ARROW, 0, 3));
                }
            }


            else{
                $level = $e->getPlayer()->getLevel();
                $level->addSound(new ClickSound($e->getPlayer()->asVector3(), 3));
                $player->sendPopup("§4Geen munitie! Je kan munitie kopen bij een gunshop");
            }
        }

    }


    public function holdItem(PlayerItemHeldEvent $e) {
        $player = $e->getPlayer();
        if($e->getItem()->getId() == ItemIds::RECORD_13 AND $e->getItem()->getCustomName() == "§4AWP"){
            $effect = new EffectInstance(Effect::getEffect(Effect::SLOWNESS), 1000000000, 3, false);
            $player->addEffect($effect);
            $level = $e->getPlayer()->getLevel();
            $x = $e->getPlayer()->getX();
            $y = $e->getPlayer()->getY();
            $z = $e->getPlayer()->getZ();
            $pos = new Vector3($x, $y, $z);
            $level->addSound(new PopSound($pos, 3));

        }
        if($e->getItem()->getId() != ItemIds::RECORD_13 OR $e->getItem()->getCustomName() != "§4AWP"){
            $player->removeEffect(Effect::SLOWNESS);
        }
    }

    public function entityhitevent(ProjectileHitEntityEvent $e){
        $vector3 = $e->getEntity()->asVector3();
        $e->getEntity()->getLevel()->addParticle(new RedstoneParticle($vector3, 1.5));

    }

    public function hitevent(ProjectileHitEvent $e){
        $e->getEntity()->flagForDespawn();
    }
}


 
